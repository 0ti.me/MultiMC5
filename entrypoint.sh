#!/usr/bin/env bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

main() {
  set -e

  pwd
  ls -alh

  mkdir -p build install
  cd build

  cmake -DCMAKE_INSTALL_PREFIX=../install ..

  make -j$(nproc --all) install
}

main "$@"
