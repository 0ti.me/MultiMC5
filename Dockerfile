FROM ubuntu:19.04

RUN apt update \
  && apt install -qy \
    libgl1-mesa-dev \
    openjdk-8-jdk \
    zlib1g-dev

RUN echo \
 && apt install --no-install-recommends -qy \
    build-essential \
    cmake

RUN apt install --no-install-recommends -qy \
  qt5-default

ENV \
  USER=build \
  USERID=1000 \
  GROUPID=1000

ENV \
  BUILD_HOME=/home/${USER} \
  GROUP=${USER}

RUN \
  groupadd \
    --gid "${GROUPID}" \
    "${USER}" \
  && useradd \
    --create-home \
    --gid "${GROUPID}" \
    --home "${BUILD_HOME}" \
    --shell /bin/bash \
    --uid "${USERID}" \
    "${USER}" \
  && chown -R "${USER}:${GROUP}" "${BUILD_HOME}"

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh

USER "${USER}"
WORKDIR "${BUILD_HOME}"
